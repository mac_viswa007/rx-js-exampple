import {
  Observable,
  fromEvent,
  combineLatest,
  interval,
  of,
  forkJoin,
  throwError,
  zip,
  pipe,
  iif,
  defer,
  Subject,
  from,
  ConnectableObservable,
  BehaviorSubject,
  ReplaySubject,
  AsyncSubject,
  asyncScheduler
} from "rxjs";
import {
  mapTo,
  scan,
  startWith,
  concat,
  map,
  concatAll,
  mergeMap,
  delay,
  tap,
  takeUntil,
  takeWhile,
  exhaustMap,
  finalize,
  repeat,
  multicast,
  refCount,
  observeOn
} from "rxjs/operators";
import { resolve } from "url";
const exObservable = Observable.create((observer: any) => {
  observer.next("hello world");
  observer.next("Hi to all");
  observer.complete();
  observer.next("bye");
});

exObservable.subscribe(
  (x: any) => logItem(x),
  (error: any) => logItem(`Error: ${error}`),
  () => logItem("completed")
);

function logItem(x: any) {
  let nodeLi = document.createElement("li");
  let textNode = document.createTextNode(x);
  nodeLi.appendChild(textNode);
  document.getElementById("list").appendChild(nodeLi);
}

//========================================combine latest ================================

let redTotal = document.getElementById("red-total");
let blackTotal = document.getElementById("black-total");
let blueTotal = document.getElementById("blue-total");

let total = document.getElementById("total");

const addClickId = (id: any) => {
  return fromEvent(document.getElementById(id), "click").pipe(
    mapTo(1),
    scan((acc, curr) => acc + curr, 0),
    startWith(0)
  );
};

combineLatest(
  addClickId("red"),
  addClickId("black"),
  addClickId("blue")
).subscribe(([red, black, blue]: any) => {
  redTotal.innerHTML = red;
  blackTotal.innerHTML = black;
  blueTotal.innerHTML = blue;
  total.innerHTML = red + black + blue;
});

//==============================================concate=================================
const samplePromise = (val: any) => new Promise(resolve => resolve(val));
const source = interval(2000);
const concatallEx = source.pipe(
  map((val: any) => samplePromise(val)),
  concatAll()
);

//const subscribeConcatall = concatallEx.subscribe(value => {
// console.log(`promise return value concatall : ${value}`);
//});
//==================================fork join======================================
//when all observables completes, emit the last emitted value from each

const emiitedPromise = (val: any) =>
  new Promise(resolve =>
    setTimeout(() => resolve(`Promise resolved ${val}`), 5000)
  );

const sourceVal = of([1, 2, 3, 4, 5]);
const forkJoinEX = sourceVal.pipe(
  mergeMap((q: any) => forkJoin(...q.map(emiitedPromise)))
);
const subscribeforkJoinPromise = forkJoinEX.subscribe(val => console.log(val));

const forJoinWithCatchError = forkJoin(
  of("Hello world"),
  of("This is mahesh").pipe(delay(1000)),
  of("how do we communicate").pipe(delay(2000)),
  throwError("this will error")
);

const subscribeForJoinWithError = forJoinWithCatchError.subscribe(val =>
  console.log(val)
);
//===========================================zip==============================================
const eventTime = (eventName: any) =>
  fromEvent(document, eventName).pipe(map(() => new Date()));
const zipOperator = zip(eventTime("mousedown"), eventTime("mouseup")).pipe(
  map(([start, end]) => Math.abs(start.getTime() - end.getTime()))
);
zipOperator.subscribe(console.log);
//============= swipe to refresh example=============================================

const setRefreshPos = (y: any) =>
  (document.getElementById("refresh").style.top = `${y}px`);
const resetRefresh = () => setRefreshPos(10);
const setData = (data: any) =>
  (document.getElementById("data").innerText = data);
const fakeRequest = () =>
  of(new Date().toUTCString()).pipe(
    tap(_ => console.log("request")),
    delay(1000)
  );

const takeUntilMouseUpOrRefresh = pipe(
  takeUntil(fromEvent(document, "mouseup")),
  takeWhile((y: any) => y < 110)
);

const moveDot = (y: any) => of(y).pipe(tap(setRefreshPos));
const refresh = of({}).pipe(
  tap(resetRefresh),
  tap(e => setData(".... refreshing ....")),
  exhaustMap(_ => fakeRequest()),
  tap(setData)
);

fromEvent(document, "mousedown")
  .pipe(
    mergeMap(_ => fromEvent(document, "mousemove")),
    map((e: MouseEvent) => e.clientY),
    takeUntilMouseUpOrRefresh,
    finalize(resetRefresh),
    exhaustMap((y: any) => iif(() => y < 100, moveDot(y), refresh)),
    finalize(() => console.log("end")),
    repeat()
  )
  .subscribe();
//===============================================================================
//RxJs subject observables

const sourceObservable = defer(() => of(Math.floor(Math.random() * 100)));

const observer = (name: string) => {
  return {
    next: (value: any) => console.log(`observer ${name}: ${value}`),
    complete: () => console.log(`observer ${name} : completed`)
  };
};

const subject = new Subject<number>();
subject.subscribe(observer("a"));
subject.subscribe(observer("b"));
sourceObservable.subscribe(subject);

function multicastt<T>(source: Observable<T>) {
  const subject = new Subject<T>();
  source.subscribe(subject);
  return subject;
}

// subject inside function seems like subject receive next and complete notification before observer is subscribed
// observer gets only complete notification
const muticastSubject = multicastt(sourceObservable);
muticastSubject.subscribe(observer("a"));
muticastSubject.subscribe(observer("b"));
// to avoid above problem : Caller of the any function connects to multicating infrastructure needs to be control
//when the subject subscribes the source
// connected Observerable
const sourceObs = of(1);
const multiConnectedObservable = sourceObs.pipe(
  multicast(new Subject<Number>())
) as ConnectableObservable<number>;
multiConnectedObservable.subscribe(observer("a"));
multiConnectedObservable.subscribe(observer("b"));
multiConnectedObservable.connect();

// ====================================================================================
//to print letterArray = ['A','B','C','D','E','F'] from given html element
const element = document.getElementsByClassName("select-container");
const element0: any = element[0];
const a: any = element0.dataset.letter;
const b: any = element0.innerText;
const bArray = b.split(/\n/);
bArray.sort();
const letterArray = [a, ...bArray];
console.log(element, a, b, letterArray);
//=======================================================================================
//refcount operator : makes multicasted observable automatically start executing when the first
//subscriber arrives and stop when last subscriber leaves

const sourceO = interval(1000);
const refCounted = sourceO.pipe(
  multicast(() => new Subject()),
  refCount()
);

let sub1: any, sub2: any;
sub1 = refCounted.subscribe(observer("a"));

setTimeout(() => {
  sub2 = refCounted.subscribe(observer("b"));
}, 600);

setTimeout(() => {
  sub1.unsubscribe();
}, 1200);

setTimeout(() => {
  sub2.unsubscribe();
}, 2000);
//=======================================================================================
/*Behaviour subject ==> one of the varient of the subjects
it holds  current value means if new observer arrives, it will immediately receive the current
value from the BehaviourSubject
*/

const bSubject = new BehaviorSubject(0);
bSubject.subscribe(observer("a"));

bSubject.next(1);
bSubject.next(2);

bSubject.subscribe(observer("b"));

bSubject.next(3);
//======================================================================================
/**
 * Replay Subject : can send old values to the new subscribers
 * can also records a part of the observable execution
 * constructor(parameter): last number of buffer values
 */

const replaySubject = new ReplaySubject(3);

replaySubject.subscribe(observer("a"));

replaySubject.next(1);
replaySubject.next(2);
replaySubject.next(3);
replaySubject.next(4);

replaySubject.subscribe(observer("b"));
replaySubject.next(5);
//=====================================================================================
/**
 * 
 * Async subject :where only the last value of the observable execution is sent to its observers 
 * and only when execution completes
 
 */
const asyncSubject = new AsyncSubject();

asyncSubject.subscribe(observer("a"));

asyncSubject.next(1);
asyncSubject.next(2);
asyncSubject.next(3);
asyncSubject.next(4);

asyncSubject.subscribe(observer("b"));
asyncSubject.next(5);
asyncSubject.complete();
//====================================================================================
/**
 *Scheduler:
 * 1) data structure and it konws how to store and queue tasks based on priority
 * 2) is execution context. it denotes where and when the task is executed
 * 3) has (virtual) clock. it provides notion of time by a getter method now()
 *
 * lets you define in what execution context will an Observable deliver notifications to its observer
 */

const observable = new Observable(observer => {
  observer.next(1);
  observer.next(2);
  observer.next(3);
  observer.complete();
}).pipe(observeOn(asyncScheduler));

console.log("observer before subscribe");

observable.subscribe(observer("Async schedular"));
console.log("observer after subscribe");

/**
 * scheduler with proxyObserver
 */
const proxyobserver = (name: string) => {
  return {
    next(val: any) {
      asyncScheduler.schedule(x => observer(name).next(x), 0, val);
    },
    complete: () => console.log(`proxyObserver : completed`)
  };
};
const proxyObserver = new Observable(proxyObserver => {
  proxyObserver.next(1);
  proxyObserver.next(2);
  proxyObserver.next(3);
  proxyObserver.complete();
}).pipe(observeOn(asyncScheduler));

console.log("observer before subscribe");
observable.subscribe(proxyobserver("Async schedular with proxy observer"));
console.log("observer after subscribe");
